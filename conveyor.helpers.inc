<?php
/**
 * @file
 * Basic much-used helper methods that make it easier for you.
 */

/**
 * Logs in into the endpoint id.
 */
function _conveyor_login($eid) {
  if (is_numeric($eid)) {
    $endpoint = _conveyor_get_endpoint($eid);
    $cookie_file = _conveyor_get_endpoint_cookie_file($eid);

    // Find restful endpoint.
    $user_data = array(
      'username' => $endpoint['username'],
      'password' => $endpoint['password'],
    );

    $login_url = $endpoint['uri'] . '/' . $endpoint['endpoint'] . '/user/login';
    // Start clean session.
    if (file_exists($cookie_file)) {
      unlink($cookie_file);
    }

    // Initialize call.
    $call = curl_init();
    $curl_options = array(
      CURLOPT_URL => $login_url,
      CURLOPT_HEADER => 0,
      CURLOPT_FOLLOWLOCATION => 1,
      CURLOPT_HTTPHEADER => array('Accept: application/json'),
      CURLOPT_POST => 1,
      CURLOPT_POSTFIELDS => $user_data,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_COOKIEJAR => $cookie_file,
      CURLOPT_COOKIEFILE => $cookie_file,
    );
    curl_setopt_array($call, $curl_options);

    $login_response = curl_exec($call);
    $http_code = curl_getinfo($call, CURLINFO_HTTP_CODE);

    // Return TRUE if the login succeeded and the code is 200.
    if ($login_response && $http_code == 200) {
      watchdog('conveyor', t('Logged in successfully'));
      if (variable_get('conveyor_verbose', 0)) {
        drupal_set_message(t("Successfully logged in to !login_url", array('!login_url' => $login_url)));
      }
      return TRUE;
    }
    else {
      watchdog('conveyor', t('Failed to login'));
      return FALSE;
    }
  }
}

/**
 * Gets cookie file (currently unused).
 */
function _conveyor_get_endpoint_cookie_file($eid) {
  return sys_get_temp_dir() . '/' . $eid . ".txt";
}

/**
 * Fetch all config from the Drupal variables.
 */
function _conveyor_get_all_config() {
  $or = db_or()
    ->condition('name', 'conveyor_content_type_%_fields', 'LIKE')
    ->condition('name', 'conveyor_content_type_%', 'LIKE');
  $select = db_select('variable', 'v')
    ->fields('v')
    ->condition($or)
    ->execute();

  return $select->fetchAll();
}

/**
 * Get a theme('table') format of endpoints.
 */
function _conveyor_get_endpoints_table() {
  $endpoints = _conveyor_get_endpoints();
  $header = array(
    t('Name'),
    t('URI'),
    t('Username'),
    t('Password'),
    t('Actions'),
  );

  $rows = array();
  foreach ($endpoints as $endpoint) {
    $endpoint->password = '******';
    $rows[] = array(
      $endpoint->name,
      $endpoint->uri,
      $endpoint->username,
      $endpoint->password,
      l(t('Edit'), 'admin/config/system/conveyor/endpoint/' . $endpoint->eid . '/edit',
        array('attributes' => array('class' => array('button')))) . ' ' .
      l(t('Delete'), 'admin/config/system/conveyor/endpoint/' . $endpoint->eid .
        '/delete/confirm', array('attributes' => array('class' => array('button')))),
    );
  }
  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => NULL,
  ));
}

/**
 * Returns form element options with all exportable endpoints.
 */
function _conveyor_get_endpoints_options() {
  $endpoints = _conveyor_get_endpoints();
  $options = array();
  foreach ($endpoints as $endpoint) {
    $options[$endpoint->eid] = $endpoint->name;
  }
  return $options;
}

/**
 * Fetches endpoints and orders by name.
 */
function _conveyor_get_endpoints() {
  $endpoints_result = db_select('conveyor_endpoints', 'e')
    ->fields('e')
    ->orderBy('name', 'ASC')
    ->execute();
  return $endpoints_result->fetchAll();
}

/**
 * Fetches endpoint data given the endpoint id.
 */
function _conveyor_get_endpoint($eid) {
  $endpoint_result = db_select('conveyor_endpoints', 'e')
    ->fields('e')
    ->condition('eid', $eid, '=')
    ->execute();
  $endpoint = $endpoint_result->fetchAssoc();
  return $endpoint;
}

/**
 * Converts a UUID to a given ID.
 */
function _conveyor_get_id_by_uuid($uuid) {
  $nid = entity_get_id_by_uuid('node', array($uuid));
  if (!empty($nid)) {
    return array_shift($nid);
  }
  else {
    return NULL;
  }
}

/**
 * Serializes and base64's an object for transport.
 */
function conveyor_encode_object($object) {
  return strtr(base64_encode(serialize($object)), '+/=', '-_,');
}

/**
 * Unserializes and base64_decodes an object at recipient's side.
 */
function conveyor_decode_object($string) {
  return unserialize(base64_decode(strtr($string, '-_,', '+/=')));
}

/**
 * Temporary function of currently implemented fields.
 */
function _conveyor_implemented_widgets() {
  return array(
    'text_textarea_with_summary',
    'text_textfield',
  );
}
