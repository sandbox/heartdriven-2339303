Conveyor provides a way to push and synchronize content and configuration from
one environment to another. This tackles a common problem that content cannot
be migrated across different drupal instances (e.g test, staging, production).

In the easiest setup with 2 drupal instances, there's a sending end (origin)
and a receiving end (destination). The destination has to be set up with a REST
service using the Services module. On the sending end the credentials of the
origin Drupal instance have to be configured, such as the url, endpoint, user
login and password.

On the origin environment every node is provided with a 'push' tab. If you click
it, that node is pushed to any of the endpoints you choose (in this case, 1).
Nodes are given a unique identifier using the UUID module and are synchronized
when pushing a given node multiple times.

# Requirements

UUID
Services 3 and the rest_server submodule
Entity
Known problems

As for now, the conveyor module doesn't cover everything. I'm currently
refactoring less-presentable code into coding standards and best practices. A
bit lower on this page there's a list of priorities that have proved to work
well on a production site.

# Installation

Conveyor has to be installed and enabled on each website. Once all the
dependencies are installed, create a new service on the receiving end in
admin/structure/services. Choose REST and name it whatever you want. Once
saved, enable the following settings for that service:

## Tab: Server
Response formatters: json
Request parsing: all options

## Tab: Resources
Check the conveyor_config tickbox.
Check the conveyor_node tickbox.
Check the user tickbox.

Finally, go to the module administration page (admin/config/system/conveyor)
and check any field you want to import. Select the environment you want to push
it to, and click 'Save'. Note, not all the fields are implemented yet.
