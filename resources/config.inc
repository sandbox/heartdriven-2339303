<?php
/**
 * @file
 * Variable logic for configuration variables.
 */

/**
 * Retrieves all configuration variables for Conveyor fields & types.
 */
function conveyor_config_resource_retrieve() {
  return _conveyor_get_all_config();
}

/**
 * Sets a single config variable.
 */
function conveyor_config_resource_push($config_data) {
  $incoming_config = conveyor_decode_object($config_data['config']);
  foreach ($incoming_config as $conf) {
    variable_set($conf->name, unserialize($conf->value));
  }
  return $conf->name;
}
