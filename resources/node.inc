<?php
/**
 * @file
 * Everything concerning pushing node stuff.
 */

/**
 * Retrieving conveyor node, a serialized PHP object.
 */
function conveyor_node_resource_retrieve($uuid) {
  $nid = _conveyor_get_id_by_uuid($uuid);
  if (!is_null($nid)) {
    $node_object = node_load($nid);
    return serialize($node_object);
  }
  else {
    return array(
      'messages' => 'Node does not exist yet',
    );
  }
}

/**
 * Endpoint for receiving a node push (endpoint).
 */
function conveyor_node_resource_push($uuid, $node_data) {
  // Decode the incoming string.
  $incoming_node = conveyor_decode_object($node_data['node']);

  // Check if node already exists.
  $local_node = entity_uuid_load('node', array($incoming_node->uuid));

  // Getting exportable fields, defined in the admin section.
  $exportable_fields = variable_get('conveyor_content_type_fields_' .
    $incoming_node->type, FALSE);

  // Node does not exist, creating new.
  if (empty($local_node)) {
    $entity_params = array(
      'is_new' => TRUE,
      'bundle' => 'node',
      'type' => $incoming_node->type,
      'title' => $incoming_node->title,
      'uuid' => $incoming_node->uuid,
    );
    $local_node = entity_create('node', $entity_params);
  }
  // Node exists, using existing.
  else {
    $local_node = array_pop($local_node);
    $incoming_node->nid = $local_node->nid;
    $incoming_node->is_new = FALSE;
    $incoming_node->vid = $local_node->vid;

    // Prepare destination node by unsetting the key of the exportable field.
    foreach ($exportable_fields as $exportable_field) {
      unset($local_node->{$exportable_field});
    }
  }

  // Updating the main attributes of node.
  $updatable_attributes = array(
    'title',
    'uid',
    'created',
    'changed',
    'language',
    'status',
  );
  foreach ($updatable_attributes as $attribute) {
    $local_node->{$attribute} = $incoming_node->{$attribute};
  }

  foreach ($incoming_node as $key => &$field_items) {
    // If field is in the exportable_fields array, then export the field.
    if (!empty($exportable_fields) && in_array($key, $exportable_fields)) {
      foreach ($field_items as $field_item) {
        // Going over each value of field item (e.g. $node->field_x['und'][]).
        foreach ($field_item as $field_item_value) {
          // Making easy booleans in order to define the field type.
          $is_textfield = !empty($field_item_value['value']);
          $is_link = !empty($field_item_value['url']);

          // Textfield.
          if ($is_textfield || $is_link) {
            // Changed node_destination to $local_node.
            $local_node->{$key} = $incoming_node->{$key};
          }
        }
      }
    }
  }

  try {
    node_save($local_node);
    return TRUE;
  }
  catch (Exception $e) {
    watchdog('conveyor', 'Something went wrong while saving a pushed node.');
  }
  return FALSE;
}
