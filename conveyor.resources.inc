<?php
/**
 * @file
 * Resources, or endpoint methods that spew XML or JSON.
 */

/**
 * Implements hook_services_resources().
 */
function conveyor_services_resources() {
  return array(
    'conveyor_config' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => 'Retrieves a config variable',
          'callback' => 'conveyor_config_resource_retrieve',
          'access arguments' => array('access content'),
        ),
      ),
      'actions' => array(
        'pushconfig' => array(
          'help' => 'Processes a config array',
          'callback' => 'conveyor_config_resource_push',
          'access arguments' => array('access content'),
          'args' => array(
            array(
              'name' => 'config',
              'type' => 'string',
              'description' => t('the config array'),
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),
    'conveyor_node' => array(
      'retrieve' => array(
        'help' => 'Retrieves a node',
        'callback' => 'conveyor_node_resource_retrieve',
        'access arguments' => array('access content'),
        'args' => array(
          array(
            'name' => 'uuid',
            'type' => 'string',
            'description' => 'The UUID of the note to get',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
        ),
      ),
      'update' => array(
        'callback' => 'conveyor_node_resource_push',
        'access arguments' => array('access content'),
        'args' => array(
          array(
            'name' => 'uuid',
            'type' => 'string',
            'description' => 'The UUID of the node to push',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
          array(
            'name' => 'node',
            'type' => 'string',
            'description' => t('the node data to push'),
            'source' => 'data',
            'optional' => FALSE,
          ),
        ),
        'help' => 'Pushes a node',
      ),
    ),
  );
}
