<?php
/**
 * @file
 * Logic for submitting config.
 */

/**
 * Conveys node over the restful interface.
 */
function _conveyor_put_config($eid) {
  $data = array('config' => conveyor_encode_object(_conveyor_get_all_config()));
  $endpoint = _conveyor_get_endpoint($eid);
  $url = $endpoint['uri'] . '/' . $endpoint['endpoint'] . '/conveyor_config/pushconfig.json';

  // Log in into the system.
  _conveyor_login($eid);

  // Setting up CURL session.
  $call = curl_init();
  $curl_options = array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => http_build_query($data),
  );
  curl_setopt_array($call, $curl_options);

  return curl_exec($call);
}
