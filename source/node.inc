<?php
/**
 * @file
 * Logic for transmitting a given node towards another endpoint.
 */

/**
 * Provides a form in which a user can choose where to push the node to.
 */
function conveyor_node_push($nid) {
  $form = array();
  $form['endpoints'] = array(
    '#title' => t('Where do you want to convey this !type to?', array('!type' => 'node')),
    '#type' => 'checkboxes',
    '#options' => _conveyor_get_endpoints_options(),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#value' => t('Convey this !type', array('!type' => 'node')),
    '#type' => 'submit',
  );
  $form['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'node/' . arg(1)),
  );
  return $form;
}

/**
 * What happens AFTER an endpoint has been selected.
 */
function conveyor_node_push_submit($form, &$form_state) {
  $endpoints = array_values($form_state['values']['endpoints']);
  foreach ($endpoints as $eid) {
    if ($eid !== 0) {
      $endpoint = _conveyor_get_endpoint($eid);
      $node = node_load(arg(1));

      if (_conveyor_put_node($eid, $node)) {
        drupal_set_message(t('Pushed node with uuid %uuid to endpoint %endpoint', array(
          '%uuid' => $node->uuid,
          '%endpoint' => $endpoint['name'])
        ), 'status');
      }
      else {
        drupal_set_message(t('Something went wrong.  Check your settings.'), 'error');
      }
    }
  }
  drupal_goto('node/' . arg(1));
}

/**
 * Conveys node over the restful interface.
 */
function _conveyor_put_node($eid, $node) {
  $data = array('node' => conveyor_encode_object($node));
  $endpoint = _conveyor_get_endpoint($eid);
  $put_url = $endpoint['uri'] . '/' . $endpoint['endpoint'] . '/conveyor_node/' . $node->uuid . '.json';

  // Log in into the system.
  _conveyor_login($eid);

  // Setting up CURL session.
  $call = curl_init();
  $curl_options = array(
    CURLOPT_URL => $put_url,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_CUSTOMREQUEST => 'PUT',
    CURLOPT_POSTFIELDS => http_build_query($data),
  );
  curl_setopt_array($call, $curl_options);

  return curl_exec($call);
}
