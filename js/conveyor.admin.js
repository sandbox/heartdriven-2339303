/**
 * @file
 *
 * Adds javascript intelligence to Conveyor's admin form.
 */

(function ($) {
  Drupal.behaviors.conveyorAdmin = {
    attach: function (context, settings) {
      checkAllFields();
      $('.toggle-fields').on('click', function() {
        checkToggledFields($(this));
      });
      $('.vertical-tab-button a').on('click', function(){
        checkAllFields();
      });

      function checkAllFields() {
        $('.toggle-fields').each(function(){
          checkToggledFields($(this));
        });
      }

      function checkToggledFields(input) {
        if (input.prop('checked')) {
          input.parents('.fieldset-wrapper').find('.form-type-checkboxes').show(100);
        }
        else {
          input.parents('.fieldset-wrapper').find('.form-type-checkboxes').hide();
        }
      }
    }
  };
})(jQuery);
