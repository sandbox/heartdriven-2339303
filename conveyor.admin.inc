<?php
/**
 * @file
 * The admin form, providing ways to customize the Conveyor.
 */

/**
 * Conveyor admin form.
 */
function conveyor_admin() {
  drupal_add_js(drupal_get_path('module', 'conveyor') . '/js/conveyor.admin.js');
  $form = array();
  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['general_settings']['conveyor_verbose'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verbose mode'),
    '#default_value' => variable_get('conveyor_verbose', 0),
    '#description' => t('A lot more information is shown when performing the convey action.'),
  );
  $form['site_role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Assign a role to this node'),
  );
  $form['site_role']['conveyor_role'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Is this Drupal instance an origin or destination node?'),
    '#default_value' => variable_get('conveyor_role', array()),
    '#options' => array(
      'origin' => t('Origin site'),
      'destination' => t('Destination site'),
    ),
  );
  $form['endpoints'] = array(
    '#type' => 'fieldset',
    '#title' => t('Manage endpoints'),
  );
  $form['endpoints']['list'] = array(
    '#type' => 'markup',
    '#markup' => _conveyor_get_endpoints_table(),
  );
  $form['endpoints']['create_new'] = array(
    '#type' => 'markup',
    '#markup' => l(t('New endpoint'), 'admin/config/system/conveyor/endpoint/create',
      array('attributes' => array('class' => array('button')))),
  );
  $form = system_settings_form($form);

  // Field set group.
  $form['node_type_settings'] = array(
    '#type' => 'vertical_tabs',
  );

  $content_types = node_type_get_types();
  foreach ($content_types as $type) {
    // Vertical fieldset.
    $form['node_type_' . $type->type] = array(
      '#type' => 'fieldset',
      '#title' => t("%content_type settings", array('%content_type' => $type->name)),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      '#group' => 'node_type_settings',
    );
    $form['node_type_' . $type->type]['conveyor_content_type_' . $type->type] = array(
      '#type' => 'checkbox',
      '#title' => t('Make nodes of type %content_type conveyable', array(
        '%content_type' => $type->name,
        )),
      '#default_value' => variable_get(sprintf('conveyor_content_type_%s', $type->type)),
      '#attributes' => array(
        'class' => array('toggle-fields'),
      ),
    );
    // Now get the fields of this content type, put them in a checkboxes field.
    $fields = field_info_instances("node", $type->type);

    $options = array();
    foreach ($fields as $field) {
      $options[$field['field_name']] = $field['label'] . ' (' . $field['field_name'] . ')';
    }

    $form['node_type_' . $type->type][$type->type . '_exportable_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Export these fields'),
      '#options' => $options,
      '#default_value' => variable_get(sprintf('conveyor_content_type_fields_%s', $type->type), array()),
    );
  }

  $endpoint_options = _conveyor_get_endpoints_options();
  if (!empty($endpoint_options)) {
    $form['convey'] = array(
      '#title' => t('Configuration'),
      '#type' => 'fieldset',
    );
    $form['convey']['push'] = array(
      '#title' => t('Choose endpoints'),
      '#title_display' => 'none',
      '#description' => t('This will send all of the necessary settings to the
        designated environment.  Conveyor will only work if you do so!'),
      '#type' => 'checkboxes',
      '#options' => $endpoint_options,
    );
  }
  $form['#submit'][] = 'conveyor_admin_submit';
  return $form;
}

/**
 * Form submit for conveyor_admin form.
 */
function conveyor_admin_submit($form, &$form_state) {
  $values = $form_state['values'];

  $content_types = node_type_get_types();
  foreach ($content_types as $type) {
    $fields = array_values($values['node_type_' . $type->type][$type->type . '_exportable_fields']);

    variable_set('conveyor_content_type_' . $type->type, $values['node_type_' .
      $type->type]['conveyor_content_type_' . $type->type]);

    $exportable_fields = array();
    foreach ($fields as $field) {
      if ($field !== 0) {
        $exportable_fields[] = $field;
      }
    }
    variable_set('conveyor_content_type_fields_' . $type->type, $exportable_fields);
  }

  // Require 'config' source file.
  module_load_include('inc', 'conveyor', 'source/config');
  if (!empty($values['push'])) {
    foreach ($values['push'] as $selected_endpoint) {
      if ($selected_endpoint !== 0) {
        // A valid endpoint was selected, let's push now!
        _conveyor_put_config($selected_endpoint);
      }
    }
  }
}

/**
 * Form for creating or updating an endpoint.
 */
function conveyor_endpoint_form() {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Description of the endpoint'),
    '#required' => TRUE,
  );
  $form['uri'] = array(
    '#type' => 'textfield',
    '#title' => t('URI'),
    '#description' => t('Endpoint URI without trailing slash (e.g. http://mysite.com)'),
    '#required' => TRUE,
  );
  $form['endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Service endpoint'),
    '#description' => t('Endpoint of the RESTful service (no slashes before or after)'),
    '#required' => TRUE,
  );
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Admin username'),
    '#required' => TRUE,
  );
  $form['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Admin password'),
    '#required' => TRUE,
  );

  $eid = arg(5);
  if (is_numeric($eid)) {
    // Fetch endpoint data.
    $endpoint = _conveyor_get_endpoint($eid);
    // Add a default value for each of the elements above.
    foreach ($form as $key => &$form_item) {
      $form_item['#default_value'] = $endpoint[$key];
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation hook for endpoints.
 */
function conveyor_endpoint_form_validate($form, &$form_state) {
  if (substr($form_state['values']['uri'], 0, 7) != 'http://'
    && substr($form_state['values']['uri'], 0, 8) != 'https://') {
    form_set_error('uri', t("URI doesn't start with http:// or https://"));
  }
  if (strlen($form_state['values']['uri']) < 11) {
    form_set_error('uri', t("The URI is too short"));
  }
}

/**
 * Submit hook for endpoints.
 */
function conveyor_endpoint_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $endpoint['name'] = $values['name'];
  $endpoint['uri'] = $values['uri'];
  $endpoint['endpoint'] = $values['endpoint'];
  $endpoint['username'] = $values['username'];
  $endpoint['password'] = $values['password'];

  $eid = arg(5);
  if (is_numeric($eid)) {
    // Existing endpoint.
    $pk = array('eid');
    $endpoint['eid'] = $eid;
  }
  else {
    // New endpoint.
    $pk = array();
  }
  if (drupal_write_record('conveyor_endpoints', $endpoint, $pk)) {
    drupal_set_message(t('Conveyor endpoint saved'));
    drupal_goto('admin/config/system/conveyor');
  }
  else {
    drupal_set_message(t('Something went wrong saving the endpoint. Contact your site administrator.'), 'error');
  }

}

/**
 * Returns a confirm form for deleting an endpoint from the endpoints table.
 */
function conveyor_endpoint_delete() {
  $form = confirm_form(array(), t('Are your sure you want to delete this endpoint?'), 'node/' . arg(1));
  return $form;
}

/**
 * Action after submitting confirm form.
 */
function conveyor_endpoint_delete_submit($form, &$form_state) {
  $delete = db_delete('conveyor_endpoints')
    ->condition('eid', arg(5))
    ->execute();
  if ($delete) {
    drupal_set_message(t('Endpoint deleted'), 'status');
    drupal_goto('admin/config/system/conveyor');
  }
  else {
    drupal_set_message(t('Error deleting endpoint'), 'warning');
  }
}
